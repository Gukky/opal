name := "Bytecode Infrastructure"

//version := "0.8.1" // LAST RELEASE
version := "0.9.0-SNAPSHOT"

scalacOptions in (Compile, doc) := Opts.doc.title("OPAL - Bytecode Infrastructure") 
