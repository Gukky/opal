/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.fpcf

/**
 * An entity and (optionally) a specific associated property.
 *
 * @author Michael Eichberg
 */
trait EOptionP[P <: Property] {

    /**
     * The entity.
     */
    val e: Entity

    /**
     * The property key of the optionally available property.
     */
    def pk: PropertyKey[P]

    /**
     * @return `true` if the entity is associated with a property.
     */
    def hasProperty: Boolean

    /**
     * Returns the property if it is available otherwise an `UnsupportedOperationException` is
     * thrown.
     */
    @throws[UnsupportedOperationException]("if no property is available")
    def p: P

    override def toString: String = s"EOptionP($e,$p)"
}

object EOptionP {

    def apply[P <: Property](e: Entity, pk: PropertyKey[P], pOption: Option[P]): EOptionP[P] = {
        pOption match {
            case Some(p) ⇒ EP(e, p)
            case None    ⇒ EPK(e, pk)
        }
    }
}